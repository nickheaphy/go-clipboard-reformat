# Clipboard Reformatter

This is a quick and dirty hack to reformat text on the clipboard.

Specifically it reformats Salesforce references to be suitable for pasting into emails.

I run the compiled Go application inside Automator sitting in the dock on OSX. I copy the URL to the Case to the clipboard (in the format `https://yourorghere.lightning.force.com/lightning/r/5002j00000R6DGoAAN/view?0.source=aloha`) then click the button and it is magically turned into `ref:_00D20IEE3._5002jR6DGo:ref` that I can paste into an email.

Sorted

## Compile

go build SFReformat.go