package main

import (
	//"fmt"
	"github.com/atotto/clipboard"
	"github.com/0xAX/notificator"
	"strings"
)

// Hardcoded as does not seem to change
// You need to change this for a different org than me
var orgid = "00D20IEE3"

// Left - Returns the first n characters of text
func Left(text string, n int) string {
	return string(text[:n])
}

// Right - Returns the last n characters of text
func Right(text string, n int) string {
	return string(text[len(text)-n:])
}


func main() {

	// Read the clipboard into text
	cbtext, err :=clipboard.ReadAll()
	if err != nil {
		panic(err)
	}

	// Sample - Source
	// https://yourorghere.lightning.force.com/lightning/r/5002j00000R6DGoAAN/view?0.source=aloha
	// Sample - Output
	// ef:_00D20IEE3._5002jR6DGo:ref

	//check that it is a Salesforce URL
	if strings.Contains(cbtext, "force.com/") {
		
		// "ref:_" & LEFT($Organization.Id,5) & 
		// SUBSTITUTE(RIGHT(Organization.Id,11), "0", "" ) &
		// "._" & LEFT(Id,5) &
		// SUBSTITUTE(Left(RIGHT(Id,10), 5), "0", "") &
		// RIGHT(Id,5) & ":ref"

		//pull off the case ID
		sfref := strings.Split(cbtext,"/")
		//get the case id
		caseid := sfref[len(sfref)-2]
		// strip the last three characters
		caseid = caseid[:len(caseid)-3]
		
		//reformat the number bit
		firstbit := "ref:_" + orgid + "._" + Left(caseid, 5)
		lastbit := Right(caseid,5) + ":ref"
		middlebit := strings.Replace(Left(Right(caseid,10),5),"0","", -1)

		//copy back to the clipboard
		err = clipboard.WriteAll(string(firstbit + middlebit + lastbit))
		if err != nil {
			panic(err)
		}

		//send notification
		notify := notificator.New(notificator.Options{
			DefaultIcon: "icon/default.png",
			AppName:     "SFReformat",
		})
		
		notify.Push("URL Converted to:", string(firstbit + middlebit + lastbit), "", notificator.UR_CRITICAL)
	}
}